import numpy as np
import pandas as pd
import os, random, sys, argparse
from keras.models import Sequential # feed forward networks
from keras.layers import Dense # fully connected layers
from sklearn.model_selection import train_test_split
from keras import optimizers
from keras import regularizers

sys.path.append('/vifi_lsu')
from vifi_lsu_mod import *


# Parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--iteration_steps',type=int,default=100,help='Number of steps for each iteration')      # Number of steps for each iteration
parser.add_argument('--RanMixPath',help='Path of RanMix file')      # Path of RanMix file

arguments = parser.parse_args()

steps=arguments.iteration_steps                             # Number of steps per each iteration
ranmixpath=arguments.RanMixPath                             # Path of output RanMix file

# Define mixture ratio and save it to file
#RanMix = [round(random.uniform(0.1, 1.0),2) for _ in range(10)]
#RanMix = [0.2 for _ in range(30)]
RanMix=[0.5,0.2]
with open(ranmixpath,'w') as f:
    # Add steps to each iteration
    ranmix=[str(j)+','+str(i)+os.linesep for i in RanMix for j in range(steps)]
    f.writelines(ranmix)


