import argparse, sys, requests, shutil, time

sys.path.append('/vifi_lsu')
from vifi_lsu_mod import *

data_sites={'bpm':'http://cci-vifipoc-04.uncc.edu:8080/nifi/', \
            'ive':'http://cci-vifipoc-01.uncc.edu:8080/nifi/', \
            'central':'http://cci-vifipoc-06.uncc.edu:8080/nifi/'}

# Parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--RanMixPath',help='Random Mix Path') # Random mix path
parser.add_argument('--BPM_command_file_path',help='Output file with command(s) to execute at BPM')   # Command file
parser.add_argument('--IVE_command_file_path',help='Output file with command(s) to execute at IVE')   # Command file
parser.add_argument('--BPM_input_eval_file_path',help='Input model evaluation files path for BPM')   # Model evaluation files path for BPM
parser.add_argument('--IVE_input_eval_file_path',help='Input model evaluation files path for IVE')   # Model evaluation files path for IVE
parser.add_argument('--BPM_output_eval_file_path',help='Output file path for accumulated model evaluation by BPM ')   # Model evaluation files path for BPM
parser.add_argument('--IVE_output_eval_file_path',help='Output file path for accumulated model evaluation by IVE')   # Model evaluation files path for IVE
parser.add_argument('--model_path',help='Received Model path')    # Model path
parser.add_argument('--work_dir',help='working directory for current script to help REST APIs')    # working directory for current script to help REST APIs

arguments = parser.parse_args()

RanMixPath=arguments.RanMixPath                             # Random Mix file path. Each line in this file is read in one iteration. If the file is empty, then iterations are over
bpm_commands_file=arguments.BPM_command_file_path           # Path of the commands file to be executed at BPM site
ive_commands_file=arguments.IVE_command_file_path           # Path of the commands file to be executed at IVE site
bpm_input_eval_file_path=arguments.BPM_input_eval_file_path       # Path of the input file containing model evaluation of the BPM
ive_input_eval_file_path=arguments.IVE_input_eval_file_path       # Path of the input file containing model evaluation of the IVE
bpm_output_eval_file_path=arguments.BPM_output_eval_file_path      # Path of the output file recording the accumulated model evaluation of the BPM
ive_output_eval_file_path=arguments.IVE_output_eval_file_path      # Path of the output file recording the accumulated model evaluation of the IVE
model_path=arguments.model_path                             # Model path
wd=arguments.work_dir                                       # working directory for current script to help REST APIs

bpm_commands=[]                                     # List of commands to be sent to BPM
ive_commands=[]                                     # List of commands to be sent to IVE
bpm_predict=False                                   # If true, then a predict command has been appended to the the list of bpm_commands during the training process (not the pre-training)

with open(RanMixPath,'r') as f:
    ranmix=f.readlines()

# Get current service name
servname=requests.get("http://127.0.0.1:5000/user/current_service",json={"path":os.path.join(wd,"conf.yml")})
if servname.status_code==200:
    servname=servname.json().strip()
    if str.lower(servname) in ['pre_services','post_services']:
        print("No service is running")
        sys.exit()
else:
    print("Error in retrieving service name: "+servname+os.linesep)
    sys.exit()

# Get current service iteration number
resultsmessage={"path": "conf.yml", "service": servname}
r=requests.get("http://127.0.0.1:5000/user/cur_iter_no",json=resultsmessage)
print("REST API to USER results: "+r.text)
if r.status_code==200:
    cur_iter_no=int(r.json())
else:
    print('Error in retrieving current iteration number for service '+servname+os.linesep)

# Check if there are still iterations to trigger new computations  
if ranmix:  # RanMix still has iterations
    # Do the necessary computations for current iteration
    mixstep,i=ranmix[0].strip().split(',')
    mixstep=int(mixstep.strip())    # Step value
    i=float(i.strip())              # Iteration value
    IVE_ratio = i*100
    BPM_ratio = (1-i)*100
    IVE_ratio1 = round(IVE_ratio)
    BPM_ratio1 = round(BPM_ratio)
    last_bpm_eval=''
    last_ive_eval=''
    #Existing_OR_IVE = 0 # use IVE data to train the ANN in the first epoch.
    
    if not mixstep:
        # Record commands to be sent to BPM and IVE based on step value
        bpm_commands.append('genTrain1')        # Generate training, testing, and validating datasets
        bpm_commands.append('fit,epochs=1000,batch_size=2000,mix=0')              # Initialize the ANN by training the ANN with only the existing BPM dataset for 600000 epochs    
        bpm_commands.append('sendmodel,trgt=ive')    # Send learned model to IVE data site
        bpm_commands.append('predict,IVE_ratio1='+str(IVE_ratio1+0.01)+',BPM_ratio1='+str(BPM_ratio1+0.01))
        changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
           "target": data_sites['bpm'], \
           "service": servname, \
           "results": ["bpm_commands.out","model.h5"]}
        r=requests.put("http://127.0.0.1:5000/user/changenifitransferresults",json=changeresultsmessage)
        print("REST API to BPM results: "+r.text)
        
        # Record commands to be sent to IVE:
        ive_commands.append('genTrain1')    # Generate training, testing, and validating datasets
        ive_commands.append('sendmodel,trgt=central')
        changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
           "target": data_sites['ive'], \
           "service": servname, \
           "results": ["ive_commands.out"]}
        r=requests.put("http://127.0.0.1:5000/user/changenifitransferresults",json=changeresultsmessage)
        print("REST API to IVE results: "+r.text)
        
    else:
        # Determine weather the new model will be fit at BPM or IVE
        with open(bpm_input_eval_file_path,'r') as f:
            last_bpm_eval=f.readline().strip()   # The evaluation file should have some value from the previous step (i.e., mixstep=0)
            if last_bpm_eval: last_bpm_eval=float(last_bpm_eval)
        
        with open(ive_input_eval_file_path,'r') as f:
            last_ive_eval=f.readline().strip()   # The evaluation file should have some value from the previous step (i.e., mixstep=0)
            if last_ive_eval: last_ive_eval=float(last_ive_eval)
                    
        if IVE_ratio*last_ive_eval > BPM_ratio*last_bpm_eval:  #Train with VR
            ive_commands.append('fit,epochs=1,batch_size=2000,mix=1')  
            ive_commands.append('sendmodel,trgt=bpm')  
            bpm_commands.append('sendmodel,trgt=central')
            # Change files sent to IVE
            changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
               "target": data_sites['ive'], \
               "service": servname, \
               "results": ["ive_commands.out","model.h5"]}
            r=requests.put("http://127.0.0.1:5000/user/changenifitransferresults",json=changeresultsmessage)
            print("REST API to IVE results: "+r.text)
            # Change files sent to BPM
            changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
               "target": data_sites['bpm'], \
               "service": servname, \
               "results": ["bpm_commands.out"]}
            r=requests.put("http://127.0.0.1:5000/user/changenifitransferresults",json=changeresultsmessage)
            print("REST API to BPM results: "+r.text)
        else: #Train with BPM
            bpm_commands.append('fit,epochs=1,batch_size=2000,mix=1')
            bpm_commands.append('sendmodel,trgt=ive')
            ive_commands.append('sendmodel,trgt=central')
            # Change files sent to BPM
            changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
               "target": data_sites['bpm'], \
               "service": servname, \
               "results": ["bpm_commands.out","model.h5"]}
            r=requests.put("http://127.0.0.1:5000/user/changenifitransferresults",json=changeresultsmessage)
            print("REST API to BPM results: "+r.text)            
            # Change files sent to IVE
            changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
               "target": data_sites['ive'], \
               "service": servname, \
               "results": ["ive_commands.out"]}
            r=requests.put("http://127.0.0.1:5000/user/changenifitransferresults",json=changeresultsmessage)
            print("REST API to IVE results: "+r.text)
    
    # evaluate the model and calculate MAE at all data sites
    bpm_commands.append('evaluate')
    ive_commands.append('evaluate')

    # Predict t bpm if for each specified number of iterations (e.g., 100 iterations)
    if not cur_iter_no%10:
        bpm_commands.append('predict,IVE_ratio1='+str(IVE_ratio1)+',BPM_ratio1='+str(BPM_ratio1)+'_iter_'+str(cur_iter_no))   # Prediction command should be the last command received by any data site. Therefore, the new fitted model will be used in the prediction
    
    # Predict the model at the BPM site at last step
    if len(ranmix)>1:
        nextstep,nexti=ranmix[1].strip().split(',')
        nextstep=int(nextstep.strip())
        #with open('step.out','a') as f:
         #   f.write('mixstep: '+str(mixstep)+', nextstep: '+str(nextstep)+'\n')
        if mixstep>nextstep:
         #   with open('step.out','a') as f:
          #      f.write('mixstep='+str(mixstep)+' > nextstep='+str(nextstep)+'\n')
            bpm_commands.append('predict,IVE_ratio1='+str(IVE_ratio1)+',BPM_ratio1='+str(BPM_ratio1))   # Prediction command should be the last command received by any data site. Therefore, the new fitted model will be used in the prediction

    # Add the evaluation value from previous iteration if exists
    with open(bpm_input_eval_file_path,'r') as f:
        res=f.readline().strip()
        if res:
            with open(bpm_output_eval_file_path,'a') as g:   # Add the last BPM evaluation to local file
                g.write(str(last_bpm_eval)+os.linesep)
    
    with open(ive_input_eval_file_path,'r') as f:
        res=f.readline().strip()
        if res:
            with open(ive_output_eval_file_path,'a') as g:   # Add the last BPM evaluation to local file
                g.write(str(last_ive_eval)+os.linesep)
            
    # Remove the current iteration value from the RanMix file. Thus, the next iteration will read the next line at the top of the file
    with open(RanMixPath,'w') as f:
        f.writelines(ranmix[1:])
    
    # Save the commands that will be send to different data sites in their proper files
    with open(bpm_commands_file,'w') as f:
        for i in bpm_commands:
            f.write(i+os.linesep)
            
    with open(ive_commands_file,'w') as f:
        for i in ive_commands:
            f.write(i+os.linesep)
            
else:       # Iterations are over. Tell BPM to upload the final EXCEL results to final destination
    # Make the 'stop.iterating' file to tell VIFI to stop iterating
    with open('stop.iterating','w') as f:
        pass
    
    # Tell BPM to return the final learned model and EXCEL files predictions
    bpm_commands.append('final')
    bpm_commands.append('predict,IVE_ratio1=final,BPM_ratio1=final')   # Prediction command should be the last command received by any data site. Therefore, the new fitted model will be used in the prediction
    ive_commands.append('stop')
    changeresultsmessage={"path": os.path.join(wd,"conf.yml"), \
        "service": servname, \
        "nifi":[{"target": data_sites['bpm'], \
       "results": ["bpm_commands.out","model.h5"]},
          {"target": data_sites['ive'], \
       "results": ["ive_commands.out","model.h5"]}]}
    r=requests.put("http://127.0.0.1:5000/user/changenifitransferresultsconditions",json=changeresultsmessage)
    print("REST API to BPM and IVE results: "+r.text)
   
    # Save the commands that will be send to different data sites in their proper files
    with open(bpm_commands_file,'w') as f:
        for i in bpm_commands:
            f.write(i+os.linesep)

    with open(ive_commands_file,'w') as f:
        for i in ive_commands:
            f.write(i+os.linesep)
