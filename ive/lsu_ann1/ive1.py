import argparse, sys

sys.path.append('/vifi_lsu')
from vifi_lsu_mod import *

# Parse input arguments
parser = argparse.ArgumentParser()

parser.add_argument('--data_directory', help='data directory')    # data directory
parser.add_argument('--coef', nargs='*', help='List of coefficients')    # coefficients 
parser.add_argument('--IndependentVal', nargs='+', help='Independent Value')    # Independent Value
parser.add_argument('--ContextualFactor', nargs='+', help='Contextual Factor')    # Contextual Factor
parser.add_argument('--DependentVal', nargs='+', help='DependentVal')    # dependent value
parser.add_argument('--data_inPath',help='Data in path')      # Data in path
parser.add_argument('--data_outPath',help='Data out path')    # Data out path
parser.add_argument('--mean_dataPath',help='mean data Path')  # Mean data path
parser.add_argument('--std_dataPath',help='std data Path')    # std data Path
parser.add_argument('--ContextualFactorNumPath',help='Contextual Factor Number Path') # File path for Contextual Factor Number
parser.add_argument('--Max_IndependentValPath',help='Maximum Independent Value path') # Maximum Independent Value path
parser.add_argument('--data_inShapePath',help='data_in shape Path') # data_in shape path
parser.add_argument('--model_path',help='Model path')    # Model path

arguments = parser.parse_args()

data_directory=arguments.data_directory                     # data directory
coef=arguments.coef                                         # Coefficients
IndependentVal=arguments.IndependentVal                     # Independent Value
DependentVal=arguments.DependentVal                         # dependent value
ContextualFactor=arguments.ContextualFactor                 # Contextual Factor
data_inPath=arguments.data_inPath                           # Data in path
data_outPath=arguments.data_outPath                         # Data out path
mean_dataPath=arguments.mean_dataPath                       # Mean data Path
std_dataPath=arguments.std_dataPath                         # std data Path
ContextualFactorNumPath=arguments.ContextualFactorNumPath   # Contextual Factor Number Path
Max_IndependentValPath=arguments.Max_IndependentValPath     # Maximum Independent Value path
data_inShapePath=arguments.data_inShapePath                 # data_in Shape Path
model_path=arguments.model_path                             # Model path

# Load required data and other parameters, then save them to files
data_in, data_out, mean_data, std_data, ContextualFactorNum, Max_IndependentVal = load_data(data_directory, coef, IndependentVal, ContextualFactor, DependentVal, [], [], [], [], [], []) # This line is on VIFI

np.savetxt(data_inPath,data_in)
np.savetxt(data_outPath,data_out)
np.savetxt(mean_dataPath,mean_data)
np.savetxt(std_dataPath,std_data)

with open(ContextualFactorNumPath,'w') as f:
    for i in ContextualFactorNum:
        f.write(str(i)+os.linesep)
        
with open(Max_IndependentValPath,'w') as f:
    for i in Max_IndependentVal:
        f.write(str(i)+os.linesep)    

with open(data_inShapePath,'w') as f:
    f.write(str(data_in.shape[0]))

# Produce the initial version of the model and save it
reg = 0.001 
lr_in = 0.01
model = Sequential() # standard feed forward networki
model.add(Dense(20, input_dim=len(IndependentVal + ContextualFactor), activation='relu',kernel_regularizer=regularizers.l1_l2(reg)))
#model.add(Dense(300, input_dim=len(IndependentVal + ContextualFactor), activation='relu',kernel_regularizer=regularizers.l1_l2(reg)))
model.add(Dense(20, activation='relu',kernel_regularizer=regularizers.l1_l2(reg)))
model.add(Dense(len(DependentVal), activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer=optimizers.Adam(lr=lr_in, beta_1=0.9, beta_2=0.999, epsilon=1e-8), metrics=['MSE','MAE','acc'])
model.save(model_path)
