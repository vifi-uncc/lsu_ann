import argparse, sys, requests, shutil, time

sys.path.append('/vifi_lsu')
from vifi_lsu_mod import *

# Set the URLs of the destinations
data_sites={'bpm':'http://cci-vifipoc-04.uncc.edu:8080/nifi/', \
            'ive':'http://cci-vifipoc-01.uncc.edu:8080/nifi/', \
            'central':'http://cci-vifipoc-06.uncc.edu:8080/nifi/', \
            'sftp1':'vifiuitasks-aws.uncc.edu'}
    
# Parse input arguments
parser = argparse.ArgumentParser()

parser.add_argument('--data_inPath',help='Input data path')      # Data in path
parser.add_argument('--data_outPath',help='Output data path')    # Data out path
parser.add_argument('--modelInPath',help='Input Model Path')        # Input Model path
parser.add_argument('--modelOutPath',help='Output Model Path')        # Output Model path
parser.add_argument('--epochs',type=int,default=1000,help='epochs for ANN')         # Epoch value
parser.add_argument('--batch_size',type=int,default=2000,help='batch size')      # Batch size
parser.add_argument('--command_file_path',help='File that specified which command(s) to execute')   # Command file
parser.add_argument('--eval_file_path',help='Model evaluation file path')   # Model evaluation file path
#parser.add_argument('--mean_dataPath',help='mean data Path')  # Mean data path#\
#parser.add_argument('--std_dataPath',help='std data Path')    # std data Path
#parser.add_argument('--IndependentVal', nargs='+', help='Independent Value')    # Independent Value-> In this PoC, it is the IVE independent value
#parser.add_argument('--DependentVal', nargs='+', help='Dependent Value')    # dependent value-> In this PoC, it is the IVE dependent value
#parser.add_argument('--ContextualFactor',nargs='+',help='Contextual Factor Number') # Contextual Factor Number-> In this PoC, it is the IVE contextual factor

arguments = parser.parse_args()
data_inPath=arguments.data_inPath                             # Data in path
data_outPath=arguments.data_outPath                           # Data out path
modelinpath=arguments.modelInPath                             # Input Model path
modeloutpath=arguments.modelOutPath                         # Output Model Path
epochs=arguments.epochs                                     # Epochs
batch_size=arguments.batch_size                             # Batch size
command_file_path=arguments.command_file_path               # Command file path
eval_file_path=arguments.eval_file_path                       # Evaluation file path
#mean_dataPath=arguments.mean_dataPath                       # Mean data Path
#std_dataPath=arguments.std_dataPath                         # std data Path
#IndependentVal=arguments.IndependentVal                      # Independent Value
#DependentVal=arguments.DependentVal                         # dependent value
#ContextualFactor=arguments.ContextualFactor   # Contextual Factor Number Path

# Load input and output data for training, testing .. etc
data_in=np.loadtxt(data_inPath)
data_out=np.loadtxt(data_outPath)

# Read input model
model=load_model(modelinpath)

# Load mean and standard_deviation
#mean_data=np.loadtxt(mean_dataPath)
#std_data=np.loadtxt(std_dataPath)

# Extract list of commands to execute
commands=[]
if os.path.isfile(command_file_path):
    with open(command_file_path,'r') as f:
        for l in f.readlines():
            commands.append(str.strip(l))

for l in commands:
    if 'gentrain1' in str.lower(l):
        # Generate training, testing, and validating datasets
        gentrain1res=genTrain1(data_in, data_out)
        data_X_train=gentrain1res['data_X_train']
        data_X_test_val=gentrain1res['data_X_test_val']
        data_Y_train=gentrain1res['data_Y_train']
        data_Y_test_val=gentrain1res['data_Y_test_val']
        data_X_test=gentrain1res['data_X_test']
        data_X_val=gentrain1res['data_X_val']
        data_Y_test=gentrain1res['data_Y_test']
        data_Y_val =gentrain1res['data_Y_val']
        # Save the generated data to be read by other operations if needed
        np.savetxt('data_X_train.out',data_X_train)
        np.savetxt('data_X_test_val.out',data_X_test_val)
        np.savetxt('data_Y_train.out',data_Y_train)
        np.savetxt('data_Y_test_val.out',data_Y_test_val)
        np.savetxt('data_X_test.out',data_X_test)
        np.savetxt('data_X_val.out',data_X_val)
        np.savetxt('data_Y_test.out',data_Y_test)
        np.savetxt('data_Y_val.out',data_Y_val)
        
    elif 'fit' in str.lower(l):
        # Load required data for the "fit" operation
        data_X_train=np.loadtxt('data_X_train.out')
        data_Y_train=np.loadtxt('data_Y_train.out')
        data_X_val=np.loadtxt('data_X_val.out')
        data_Y_val=np.loadtxt('data_Y_val.out')
        # Fit model
        command,epochs,batch_size,mix=l.strip().split(',')
        # Check if input parameters are direct or not
        if '=' in epochs:
            epochs=int(epochs.split('=')[1].strip())
        else:
            epochs=int(epochs)
            
        if '=' in batch_size:
            batch_size=int(batch_size.split('=')[1].strip())
        else:
            batch_size=int(batch_size)

        if '=' in mix:
            mix=int(mix.split('=')[1].strip())
        else:
            mix=int(mix)

        if data_X_train.size*data_X_train.ndim and data_Y_train.size*data_Y_train.ndim and data_X_val.size*data_X_val.ndim and \
                data_Y_val.size*data_Y_val.ndim:
            fit(model,data_X_train,data_Y_train,data_X_val,data_Y_val,epochs,batch_size,mix)
            model.save(modeloutpath)    # Save new model
        else:
            print("Error: Some or all required data, for the fit operation, are not available\n")

    elif 'evaluate' in str.lower(l):
        # Load required data
        data_X_test=np.loadtxt('data_X_test.out')
        data_Y_test=np.loadtxt('data_Y_test.out')
        # Evaluate model
        if data_X_test.size*data_X_test.ndim and data_Y_test.size*data_Y_test.ndim:
            eval_result=evaluate(model,data_X_test, data_Y_test)
            with open(eval_file_path,'w') as f:
                f.write(str(eval_result)+os.linesep)
        else:
            print('Error: Some or all required data, for the evaluation operation, are not available') 
    elif 'predict' in str.lower(l):
        # Predict the model
        command,data1_ratio,data2_ratio=l.strip().split(',')
        # Check if parameters are direct or not
        if '=' in data1_ratio:
            data1_ratio=int(data1_ratio.split('=')[1].strip())
        else:
            data1_ratio=int(data1_ratio)
            
        if '=' in data2_ratio:
            data2_ratio=int(data2_ratio.split('=')[1].strip())
        else:
            data2_ratio=int(data2_ratio)
            
        predResults=predict(model,data_X_test,mean_data,std_data,IndependentVal,ContextualFactor,DependentVal,data1_ratio,data2_ratio)

    elif 'sendmodel' in str.lower(l):
        # Send the learned model only to the specified destination using vifiClient REST APIs. Model should not be sent to any other destination
        command,trgt=l.strip().split(',')
        # Check if parameters are direct or not
        if '=' in trgt:
            trgt=trgt.split('=')[1].strip()
        trgt=trgt.split()   # In case multiple destinations are specified
        
        # Get current service name
        servname=requests.get("http://127.0.0.1:5000/user/current_service",json={"path":"conf.yml"})
        if servname.status_code==200:
            servname=servname.json().strip()
            if str.lower(servname) not in ['pre_services','post_services']:
                # Extract all NiFi transfers to either append/remove the model send to them as a result
                nifitransfersmsg={"path":"conf.yml", \
                               "service":servname}
                r=requests.get("http://127.0.0.1:5000/user/nifi_transfers",json=nifitransfersmsg)
                if r.status_code==200:
                    nifitransfers=r.json()
                    changeresultsmessage={"path": "conf.yml",  \
                                          "service": servname, \
                                          "nifi":[]}
                    # For each NiFi destination, check if it is required to send the model or not
                    for nifitransfer in nifitransfers:
                        res=nifitransfer['results']
                        if isinstance(res, dict):   # Just to make sure that results are in list
                            res=[k for k,_ in res.items()]
                        if nifitransfer['target_uri'] in [data_sites[i] for i in trgt]: # Model should be send to this destination
                            if not os.path.basename(modeloutpath) in res:   # Append output model, if does not already exist, to results of current destination
                                res.append(os.path.basename(modeloutpath))
                            # Change files sent to the target site
                            changeresultsmessage['nifi'].append({
                               "target": nifitransfer['target_uri'], \
                               "condition": "not stop_iteration", \
                               "results": res})
                        else:   # Remove model from any other destination
                            condition=nifitransfer['transfer']['condition']
                            if os.path.basename(modeloutpath) in res:
                                res.remove(os.path.basename(modeloutpath)) 
                                #condition=nifitransfer['transfer']['condition']
                                if not res: # Results are empty so far. So, no need to transfer any thing to this destination
                                    condition='never'
                            # Change files sent to the target site
                            changeresultsmessage['nifi'].append({
                               "target": nifitransfer['target_uri'], \
                               "condition": condition, \
                               "results": res})       
                    r=requests.put("http://127.0.0.1:5000/user/changenifitransferresultsconditions",json=changeresultsmessage)
                    print("REST API to BPM results: "+r.text)
                        
                else:   # Current configuration file has no NiFi transfers. So, make new ones
                    #TODO: Use REST APIs to make new NiFi transfers
                    pass
            else:
                print('No service is currently running')
        else:
            print("Error in retrieving current service name: "+str(servname.text))
    
    elif 'final' in str.lower(l):
        # Get current service name
        servname=requests.get("http://127.0.0.1:5000/user/current_service",json={"path":"conf.yml"})
        if servname.status_code==200:
            servname=servname.json().strip()
            if str.lower(servname) not in ['pre_services','post_services']:
                # Change files sent to the target site
                changeresultsmessage={"path": "conf.yml", \
                   "target": data_sites['sftp1'], \
                   "service": servname, \
                   "condition":"all"}
                r=requests.put("http://127.0.0.1:5000/user/changesftptransferresultsconditions",json=changeresultsmessage)
                print("REST API to BPM results: "+r.text)
            else:
                print('No service is currently running')
        else:
            print("Error in retrieving current service name: "+str(servname.text))
        with open('stop.iterating','w') as f:
            pass

    elif 'stop' in str.lower(l):
        with open('stop.iterating','w') as f:
            pass
