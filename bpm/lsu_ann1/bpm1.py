import argparse, os, uuid, sys
import numpy as np
sys.path.append('/vifi_lsu')
from vifi_lsu_mod import *

# Parse input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--data_directory', help='data directory')    # data directory
parser.add_argument('--coef', nargs='*', help='List of coefficients')    # coefficients 
parser.add_argument('--IndependentVal', nargs='+', help='Independent Value')    # Independent Value
parser.add_argument('--DependentVal', nargs='+', help='Dependent Value')    # dependent value
parser.add_argument('--ParameterID', nargs='+', help='ParameterID')    # Parameter ID
parser.add_argument('--ContextualFactorNumPath',help='Contextual Factor Number Path') # File path for Contextual Factor Number
parser.add_argument('--Max_IndependentValPath',help='Maximum Independent Value path') # Maximum Independent Value path
parser.add_argument('--mean_dataPath',help='mean data Path')  # Mean data path
parser.add_argument('--std_dataPath',help='std data Path')    # std data Path
parser.add_argument('--data_ShapePath',help='data shape Path') # data shape path
parser.add_argument('--data_inPath',help='Data in path')      # BPM in path
parser.add_argument('--data_outPath',help='Data out path')    # BPM out path

arguments = parser.parse_args()

data_directory=arguments.data_directory                     # data directory
coef=arguments.coef                                         # Coefficients
IndependentVal=arguments.IndependentVal                     # Independent Value
DependentVal=arguments.DependentVal                         # BPM dependent value
ParameterID=arguments.ParameterID                           # Parameter ID
ContextualFactorNumPath=arguments.ContextualFactorNumPath   # Contextual Factor Number Path
Max_IndependentValPath=arguments.Max_IndependentValPath     # Maximum Independent Value path
mean_dataPath=arguments.mean_dataPath                       # Mean data Path
std_dataPath=arguments.std_dataPath                         # std data Path
data_ShapePath=arguments.data_ShapePath                     # Data Shape Path
data_inPath=arguments.data_inPath                           # Data in path
data_outPath=arguments.data_outPath                         # Data out path

# Read required input arguments from specified files
with open(ContextualFactorNumPath,'r') as f:
    ContextualFactorNum=f.readlines()
ContextualFactorNum=[float(i) for i in ContextualFactorNum]

with open(Max_IndependentValPath,'r') as f:
    Max_IndependentVal=f.readlines()
Max_IndependentVal=[float(i) for i in Max_IndependentVal]

mean_data=np.loadtxt(mean_dataPath)

std_data=np.loadtxt(std_dataPath)

with open(data_ShapePath,'r') as f:
    data_inShape=int(f.readline())
    
# Extract required BPM data and save it for further computations
data_in, data_out = load_data(data_directory, coef, IndependentVal, [], DependentVal, ContextualFactorNum, Max_IndependentVal, ParameterID, mean_data, std_data, data_inShape)
np.savetxt(data_inPath, data_in)
np.savetxt(data_outPath,data_out)


